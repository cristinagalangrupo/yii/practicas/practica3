<?php

/* @var $this yii\web\View */
//var_dump($mensajes);
//var_dump($fechas);
$this->title = 'Práctica 3';
?>
<div class="wrapper">
   
    <?php 
    foreach ($fechas as $fecha){
        echo "<div class='fecha'>".date("d F Y",strtotime($fecha->fecha))."</div>";
       
    foreach ($mensajes as $mensaje){
        if($mensaje->fecha==$fecha->fecha){
            echo $this->render("_mensaje",[
                "mensaje"=>$mensaje
            ]);           
        
    }
        }
    }
    
    ?>
</div>
